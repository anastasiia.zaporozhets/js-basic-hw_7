"use strict"


//завдання перше
let strUser= "Кабак";

function isPalindrome(str) {
    let strClean = str.toLowerCase();
   let strReverse= str.toLowerCase().split("").reverse().join("");

    return strClean === strReverse;
}

let strPalindrome = isPalindrome(strUser);
console.log(`Цей вираз ${strUser} являється ${strPalindrome}`);


//завдання два
function verifyStrLength(str, maxLength) {
    return str.length <= maxLength;
}

//перевірка функції
let greatLength = verifyStrLength('checked string', 20);
console.log(greatLength);

console.log(verifyStrLength('checked string', 10));


//завдання три
let userBirthdayDay = prompt("Введіть дату народження в такому форматі рік місяць день", '1994 03 22');

function getFullYears(birthDate) {
    let nowDate = new Date();
    let userBirthday = new Date(birthDate);

    if (!/^\d{4} \d{2} \d{2}$/.test(birthDate) || userBirthday.getFullYear() > nowDate.getFullYear()) {
        return 'Невірний формат дати!';
    }

    if (nowDate.getMonth() < userBirthday.getMonth() ||
        (nowDate.getMonth() === userBirthday.getMonth() && nowDate.getDate() < userBirthday.getDate())) {
        return nowDate.getFullYear() - userBirthday.getFullYear() - 1;
    } else {
        return nowDate.getFullYear() - userBirthday.getFullYear();
    }
}
const userFullAge =getFullYears(userBirthdayDay);
console.log(`Зараз Вам ${userFullAge} повних рочків!`);





























